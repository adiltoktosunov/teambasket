﻿using BasketbalTeam.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketbalTeam.Models
{
    public class Person : IEntity <int>
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]

        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}
