﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BasketbalTeam.Migrations
{
    public partial class CreateCoaches : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CoachId",
                table: "People",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "YearsOfWork",
                table: "People",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_People_CoachId",
                table: "People",
                column: "CoachId");

            migrationBuilder.AddForeignKey(
                name: "FK_People_People_CoachId",
                table: "People",
                column: "CoachId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_People_People_CoachId",
                table: "People");

            migrationBuilder.DropIndex(
                name: "IX_People_CoachId",
                table: "People");

            migrationBuilder.DropColumn(
                name: "CoachId",
                table: "People");

            migrationBuilder.DropColumn(
                name: "YearsOfWork",
                table: "People");
        }
    }
}
