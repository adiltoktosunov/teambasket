﻿using BasketbalTeam.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketbalTeam.Models
{
   public class Team : IEntity<int>
    {
        public int Id { get; set; }
        public string NameClub { get; set; }
        public string City { get; set; }
        public DateTime DateOfCreate { get; set; }
        public int Wins { get; set; }
        public int Loses { get; set; }
        public List<Coach> Coaches { get; set; }
        public List<Player> Players { get; set; }
    }
}
