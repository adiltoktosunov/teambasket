﻿using BasketbalTeam.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketbalTeam.Models
{
    
    public class Coach : Person
    {
        [ForeignKey("Team")]
        public int TeamId { get; set; }
        [Required]
        public int YearsOfWork { get; set; }
        public virtual Team Team { get; set; }
    }
}
