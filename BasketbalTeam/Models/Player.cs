﻿using BasketbalTeam.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketbalTeam.Models
{
    public class Player : Person
    {
        [ForeignKey("Coach")]
        public int CoachId { get; set; }
        [ForeignKey("Team")]
        public int TeamId { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
        [Required]
        public int Number { get; set; }
        [Required]
        public Position Position { get; set; }
        public virtual Coach Coach { get; set; }
        public virtual Team Team { get; set; }
    }
}
